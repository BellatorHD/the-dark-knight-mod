package com.ovidius.tdkm.setup;

import com.ovidius.tdkm.TDKM;
import com.ovidius.tdkm.blocks.ModBlocks;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(modid = TDKM.MODID, bus = Mod.EventBusSubscriber.Bus.FORGE)

public class ModSetup {

    public static final ItemGroup itemGroup = new ItemGroup("tdkm") {
        @Override
        public ItemStack createIcon() {
            return new ItemStack(ModBlocks.BATBLOCK);
        }
    };

    public static void init() {

    }
}
