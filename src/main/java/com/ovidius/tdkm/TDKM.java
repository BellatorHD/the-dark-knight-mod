package com.ovidius.tdkm;

import com.ovidius.tdkm.blocks.BatBlock;
import com.ovidius.tdkm.blocks.BatBlockTile;
import com.ovidius.tdkm.blocks.ModBlocks;
import com.ovidius.tdkm.items.BatItem;
import com.ovidius.tdkm.setup.ClientProxy;
import com.ovidius.tdkm.setup.IProxy;
import com.ovidius.tdkm.setup.ModSetup;
import com.ovidius.tdkm.setup.ServerProxy;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.tileentity.TileEntityType;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.InterModComms;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.event.lifecycle.InterModEnqueueEvent;
import net.minecraftforge.fml.event.lifecycle.InterModProcessEvent;
import net.minecraftforge.fml.event.server.FMLServerStartingEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.stream.Collectors;

// The value here should match an entry in the META-INF/mods.toml file
@Mod(TDKM.MODID)
public class TDKM
{

    public static IProxy proxy = DistExecutor.runForDist(() -> () -> new ClientProxy(), () -> () -> new ServerProxy());

    public static ModSetup setup = new ModSetup();

    public static final Logger LOGGER = LogManager.getLogger();

    public static final String MODID = "tdkm";

    private void setup(final FMLCommonSetupEvent event) {
        setup.init();
        proxy.init();
    }

    public TDKM() {

        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::setup);

    }

    @Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
    public static class RegistryEvents {
        @SubscribeEvent
        public static void onBlockRegistry(final RegistryEvent.Register<Block> event) {
            event.getRegistry().register(new BatBlock());
        }

        @SubscribeEvent
        public static void onItemsRegistry(final RegistryEvent.Register<Item> event) {
            Item.Properties properties = new Item.Properties()
                    .group(setup.itemGroup);
            event.getRegistry().register(new BlockItem(ModBlocks.BATBLOCK, properties).setRegistryName("batblock"));
            event.getRegistry().register(new BatItem());
        }
        public static void onTileEntityRegistry(final RegistryEvent.Register<TileEntityType<?>> event) {
            event.getRegistry().register(TileEntityType.Builder.create(BatBlockTile::new, ModBlocks.BATBLOCK).build(null));
        }


}}

