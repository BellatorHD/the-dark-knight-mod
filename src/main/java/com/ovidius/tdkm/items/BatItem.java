package com.ovidius.tdkm.items;

import com.ovidius.tdkm.TDKM;
import com.ovidius.tdkm.setup.ModSetup;
import net.minecraft.item.Item;

public class BatItem extends Item {

    public BatItem(){
        super(new Item.Properties()
                .maxStackSize(16)
                .group(TDKM.setup.itemGroup));
        setRegistryName("batitem");
    }


}
