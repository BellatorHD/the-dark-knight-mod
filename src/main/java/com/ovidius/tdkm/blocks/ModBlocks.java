package com.ovidius.tdkm.blocks;

import net.minecraft.tileentity.TileEntityType;
import net.minecraftforge.registries.ObjectHolder;

public class ModBlocks {

    @ObjectHolder("tdkm:batblock")
    public static BatBlock BATBLOCK;

    @ObjectHolder("tdkm:batblock")
    public static TileEntityType<BatBlockTile> BATBLOCK_TILE;
}
